import React, {useState} from 'react';
import {Line} from 'react-chartjs-2';
import data from '../Data.json';
import moment from "react-moment";

function BarChart(props) {


//PREPARE DEFINED VALUES

    const definedValues = new Map();
    const definedKeys = [];

    for(let i = 0; i < data.length; i++){
        let start = Date.parse(data[i].startDate + " " + data[i].startTime);
        let end = Date.parse(data[i].endDate + " " + data[i].endTime);
        let difference = end - start;
        let minutesDifference = Math.floor(difference/1000/60);
        definedKeys.push(start);
        definedValues.set(start, minutesDifference);
    }
//PREPARE DEFINED VALUES

//CALCULATE RANGE BETWEEN FIRST AND LAST ITEMS AND ADD TO LIST
    let allStamps = [];

    let firstStamp = Date.parse(data[0].startDate + " " + "00:00")
    let lastStamp = Date.parse(data.slice(-1)[0].startDate + " " + "23:59")

    while (firstStamp < lastStamp){
        allStamps.push(firstStamp);
        firstStamp += 21600000;
    }

//CALCULATE RANGE BETWEEN FIRST AND LAST ITEMS AND ADD TO LIST


//LAGRANGE INTERPOLATION

    const [dataMap, setDataMap] = useState([]);

        class Data
        {
            constructor(x,y)
        {
            this.x=x;
            this.y=y;
        }
        }

        function interpolate(f,xi,n)
        {
            let result = 0; // Initialize result

            for (let i = 0; i < n; i++)
        {
            // Compute individual terms of above formula
            let term = f[i].y;
            for (let j = 0; j < n; j++)
        {
            if (j != i)
            term = term*(xi - f[j].x) / (f[i].x - f[j].x);
        }

            // Add current term to result
            result += term;
        }

            return result;
        }

    let f= []
    for(let x = 0; x < definedKeys.length; x++)
    {
        f.push(new Data(definedKeys[x], definedValues.get(definedKeys[x])))
    }

    console.log("Value of f(3) is : " +
        interpolate(f, allStamps[1], 2));

    let allData = [];

    for(let l = 0; l < allStamps.length; l++){
        allData.push(new Date(interpolate(f, allStamps[l], f.length)).getMinutes());
    }

    console.log(allData);
//LAGRANGE INTERPOLATION

//CONVERT TIMESTAMPS TO DATE FORMAT
    let allDate = []

    for(let t = 0; t < allStamps.length; t++){
        let date = new Date(+allStamps[t]);
        allDate.push(date.toDateString() + " " + date.toTimeString())
    }

    console.log(allDate)


//CONVERT TIMESTAMPS TO DATE FORMAT


    return (
        <div>
            <Line
                data={{
                    labels: allDate,
                    datasets: [{
                        label: '# of Votes',
                        data: allData,
                    }]

                }}

                height={600}
                width={400}
                options={{maintainAspectRatio: false}}

            />
        </div>
    );
}

export default BarChart;
