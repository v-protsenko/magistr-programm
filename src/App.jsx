import React from 'react';
import './App.css';
import BarChart from "./components/BarChart";

export default function App() {

    return (
      <div>
        <BarChart/>
      </div>
  );
}
